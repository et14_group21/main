#pragma once

#include <string>
#include <fstream>
#include <iostream>

class gpio_consumer
{
    public:
        gpio_consumer(int consumer);
        bool init();
        bool unexport();
        bool setStatus(std::string val);
        std::string getGpionumber();
    private:
        std::string _gpionum;
};
