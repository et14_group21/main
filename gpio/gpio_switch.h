#pragma once

#include <string>
#include <fstream>
#include <iostream>

class gpio_switch
{
    public:
        gpio_switch(int Switch);
        bool init();
        bool unexport();
        int getStatus();
        std::string getGpionumber();
    private:
        std::string _gpionum;
};
