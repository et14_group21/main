#include "gpio_switch.h"

const std::string PINS[3] = {"", "20", "21"};

/*
* constructor that takes the button number
*/
gpio_switch::gpio_switch(int Switch)
{
    _gpionum = PINS[Switch];
}

/*
* initialises the gpio input pin
* returns if initialisation was successfull
*/
bool gpio_switch::init()
{
    std::string exp = "/sys/class/gpio/export";
    std::ofstream exportgpio(exp);
    if (!exportgpio.is_open())
    {
        return false;
    }
    exportgpio << _gpionum ;
    exportgpio.close();

    std::string setdir ="/sys/class/gpio/gpio" + _gpionum + "/direction";
    std::ofstream setdirgpio(setdir);
    if (!setdirgpio.is_open())
    {
        return false;
    }
    setdirgpio << "in";
    setdirgpio.close();

    return true;
}

/*
* resets all settings made for the gpio pin
*/
bool gpio_switch::unexport()
{
    std::string unexport = "/sys/class/gpio/unexport";
    std::ofstream unexportgpio(unexport);
    if (!unexportgpio.is_open())
    {
        return false;
    }

    unexportgpio << _gpionum;
    unexportgpio.close();
    return true;
}

/*
* returns the current status of the input pin
*/
int gpio_switch::getStatus()
{
    std::string getval = "/sys/class/gpio/gpio" + _gpionum + "/value";
    std::ifstream getvalgpio(getval);
    if (!getvalgpio.is_open())
    {
        return 0;
    }
    std::string status = "";
    getvalgpio >> status;

    if(status != "0")
        status = "1";

    getvalgpio.close();
    return std::stoi(status);
}

/*
* returns the gpio number
*/
std::string gpio_switch::getGpionumber()
{
    return _gpionum;
}
