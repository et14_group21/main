#include "gpio_consumer.h"

const std::string PINS[3] = {"", "23", "24"};

/*
* constructor that takes the consumer number
*/
gpio_consumer::gpio_consumer(int consumer)
{
    unexport();
    _gpionum = PINS[consumer];
}

/*
* initialises the gpio input pin
* returns if initialisation was successfull
*/
bool gpio_consumer::init()
{
    std::string exp = "/sys/class/gpio/export";
    std::ofstream exportgpio(exp);
    if (!exportgpio.is_open())
    {
        return false;
    }
    exportgpio << _gpionum;
    exportgpio.close();

    std::string setdir ="/sys/class/gpio/gpio" + _gpionum + "/direction";
    std::ofstream setdirgpio(setdir);
    if (!setdirgpio.is_open())
    {
        return false;
    }
    setdirgpio << "out";
    setdirgpio.close();

    return true;
}

/*
* resets all settings made for the gpio pin
*/
bool gpio_consumer::unexport()
{
    std::string unexport = "/sys/class/gpio/unexport";
    std::ofstream unexportgpio(unexport);
    if (!unexportgpio.is_open())
    {
        return false;
    }

    unexportgpio << _gpionum;
    unexportgpio.close();
    return true;
}

/*
* writes a given value
*/
bool gpio_consumer::setStatus(std::string val)
{
    std::string setval = "/sys/class/gpio/gpio" + _gpionum + "/value";
    std::ofstream setvalgpio(setval);
        if (!setvalgpio.is_open())
        {
            return false;
        }

        setvalgpio << val;
        setvalgpio.close();
        return true;
}

/*
* returns the gpio number
*/
std::string gpio_consumer::getGpionumber()
{
    return _gpionum;
}
