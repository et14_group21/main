#include "switch_conf.h"

switch_conf::switch_conf(int Number, int Status, int Time)
{
    _number = Number;
    _status = Status;
    _time = Time;
}

int switch_conf::getNumber()
{
    return _number;
}

int switch_conf::getStatus()
{
    return _status;
}

int switch_conf::getTime()
{
    return _time;
}