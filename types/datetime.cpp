#include "datetime.h"

#include <iostream>
/*
* Constructor without parameters
*/
datetime::datetime()
{
    _day = 0;
    _month = 0;
    _year = 0;
    _hour = 0;
    _minute = 0;
}

/*
* Constructor without parameters
*/
datetime::datetime(int nyear, int nmonth, int nday, int nhour, int nminute)
{
    _day = nday;
    _month = nmonth;
    _year = nyear;
    _hour = nhour;
    _minute = nminute;
}

/*
* returns the mysql-usable string of this datetime object
*/
std::string datetime::toString()
{
    return std::to_string(_year) + "-" + std::to_string(_month) + "-" + std::to_string(_day) + " " + std::to_string(_hour) + ":" + std::to_string(_minute) + ":00";
}

/*
* returns this datetime as struct tm
*/
std::tm datetime::toTimeStruct()
{
    std::tm timeStruct = {0};
    timeStruct.tm_hour = _hour;
    timeStruct.tm_min = _minute;
    timeStruct.tm_sec = 0;
    timeStruct.tm_year = _year-1900;
    timeStruct.tm_mon = _month-1;
    timeStruct.tm_mday = _day;
    return timeStruct;
};

/*
* sets the attributes of this datetime with a given tm struct
*/
void datetime::OverwriteDatetime(std::tm timeStruct)
{
    setYear(timeStruct.tm_year+1900);
    setMonth(timeStruct.tm_mon+1);
    setDay(timeStruct.tm_mday);
    setHour(timeStruct.tm_hour);
    setMinute(timeStruct.tm_min);
}

/*
* adds a given number of minutes to this datetime
*/
void datetime::addMinutes(int mintoadd)
{
    tm time = toTimeStruct();
    time.tm_min += mintoadd;
    time_t t = mktime(&time);
    OverwriteDatetime(*localtime(&t));
}

/*
* adds a given number of hours to this datetime
*/
void datetime::addHours(int hourtoadd)
{
    tm time = toTimeStruct();
    time.tm_hour += hourtoadd;
    time_t t = mktime(&time);
    OverwriteDatetime(*localtime(&t));
}

/*
* adds a given number of days to this datetime
*/
void datetime::addDays(int daytoadd)
{
    tm time = toTimeStruct();
    time.tm_mday += daytoadd;
    time_t t = mktime(&time);
    OverwriteDatetime(*localtime(&t));
}

/*
* adds a given number of weeks to this datetime
*/
void datetime::addWeeks(int weektoadd)
{
    addDays(7*weektoadd);
}

/*
* adds a given number of month to this datetime
*/
void datetime::addMonth(int monthtoadd)
{
    tm time = toTimeStruct();
    time.tm_mon += monthtoadd;
    time_t t = mktime(&time);
    OverwriteDatetime(*localtime(&t));
}

/*
* adds a given number of years to this datetime
*/
void datetime::addYears(int yeartoadd)
{
    tm time = toTimeStruct();
    time.tm_year += yeartoadd;
    time_t t = mktime(&time);
    OverwriteDatetime(*localtime(&t));
}

/*
* returns datetime with diff till curent time
*/
datetime datetime::tillNow()
{
    datetime _now = getCurrent();
    datetime _diff = *this - _now;
    return _diff;
}

/*
* defines the "<" operator for datetime objects
*/
bool datetime::operator<(const datetime& d)
{
    if(_year < d._year)
        return true;
    else if (_year > d._year)
        return false;
    else
    {
        if(_month < d._month)
            return true;
        else if (_month > d._month)
            return false;
        else
        {
            if(_day < d._day)
                return true;
            else if (_day > d._day)
                return false;
            else
            {
                if(_hour < d._hour)
                    return true;
                else if (_hour > d._hour)
                    return false;
                else
                {
                    if(_minute < d._minute)
                        return true;
                    else
                        return false;
                }
            }
        }
    }
}

/*
* defines the ">" operator for datetime objects
*/
bool datetime::operator>(const datetime& d)
{
    if(_year > d._year)
        return true;
    else if (_year < d._year)
        return false;
    else
    {
        if(_month > d._month)
            return true;
        else if (_month < d._month)
            return false;
        else
        {
            if(_day > d._day)
                return true;
            else if (_day < d._day)
                return false;
            else
            {
                if(_hour > d._hour)
                    return true;
                else if (_hour < d._hour)
                    return false;
                else
                {
                    if(_minute > d._minute)
                        return true;
                    else
                        return false;
                }
            }
        }
    }
}

/*
* defines the ">=" operator for datetime objects
*/
bool datetime::operator>=(const datetime& d)
{
    return !operator<(d);
}

/*
* defines the "<=" operator for datetime objects
*/
bool datetime::operator<=(const datetime& d)
{
    return !operator>(d);
}

/*
* defines the "-" operator for datetime objects
*/
datetime datetime::operator-(const datetime& d)
{
    int dyear = _year - d._year;
    int dmonth = _month - d._month;
    int dday = _day - d._day;
    int dhour = _hour - d._hour;
    int dminute = _minute - d._minute;
    datetime dtime(dyear, dmonth, dday, dhour, dminute);

    return dtime;
}

/*
* static member function that returns current date and time as a datetime
*/
datetime datetime::getCurrent()
{
    time_t akttime = time(NULL);
    std::tm now = *localtime(&akttime);
    datetime _tnow(now.tm_year+1900, now.tm_mon+1, now.tm_mday, now.tm_hour, now.tm_min);
    return _tnow;
}

int datetime::getYear()
{
    return _year;
}

int datetime::getMonth()
{
    return _month;
}

int datetime::getDay()
{
    return _day;
}

int datetime::getHour()
{
    return _hour;
}

int datetime::getMinute()
{
    return _minute;
}

void datetime::setYear(int nyear)
{
    _year = nyear;
}

void datetime::setMonth(int nmonth)
{
    _month = nmonth;
}

void datetime::setDay(int nday)
{
    _day = nday;
}

void datetime::setHour(int nhour)
{
    _hour = nhour;
}

void datetime::setMinute(int nminute)
{
    _minute = nminute;
}

