#include "error.h"

/*
* Constructor without parameters
*/
error::error()
{
    _type = 0;
    _name = "";
    _code = 0;
}

/*
* Constructor with parameters
*/
error::error(datetime ntimestamp, int ntype, std::string nname, int ncode)
{
    _timestamp = ntimestamp;
    _type = ntype;
    _name = nname;
    _code = ncode;
}

/*
* Constructor with all parameters but the timestamp
*/
error::error(int ntype, std::string nname, int ncode)
{
    _timestamp = datetime::getCurrent();
    _type = ntype;
    _name = nname;
    _code = ncode;
}


datetime error::getTimestamp()
{
    return _timestamp;
}


int error::getType()
{
    return _type;
}


std::string error::getName()
{
    return _name;
}


int error::getCode()
{
    return _code;
}
