#include "date.h"

/*
* Constructor without parameters for empty date
*/
date::date()
{
    _id = 0;
    _consumer = 0;
    _comment = "";
    _type = 0;
    _repeat = 0;
    _is_empty = true;
}

/*
* Constructor without ID for writing
*/
date::date(datetime nstart, datetime nend, int nconsumer, std::string ncomment, int ntype, int nrepeat)
{
    _start = nstart;
    _end = nend;
    _consumer = nconsumer;
    _comment = ncomment;
    _type = ntype;
    _repeat = nrepeat;
    _is_empty = false;
}

/*
* Constructor with all parameters
*/
date::date(int nID, datetime nstart, datetime nend, int nconsumer, std::string ncomment, int ntype, int nrepeat)
{
    _start = nstart;
    _end = nend;
    _consumer = nconsumer;
    _comment = ncomment;
    _type = ntype;
    _repeat = nrepeat;
    _is_empty = false;
}

/*
* returns if this date object is empty
*/
bool date::isEmpty()
{
    return _is_empty;
}

/*
* shifts the date at given minutes
*/
void date::shift(int minutes)
{
    addMinutes(minutes);
}

/*
* adds a given number of minutes to this times
*/
void date::addMinutes(int nminute)
{
    _start.addMinutes(nminute);
    _end.addMinutes(nminute);
}

/*
* adds a given number of hours to this times
*/
void date::addHours(int nhour)
{
    _start.addHours(nhour);
    _end.addHours(nhour);
}

/*
* adds a given number of days to this times
*/
void date::addDays(int nday)
{
    _start.addDays(nday);
    _end.addDays(nday);
}

/*
* adds a given number of weeks to this times
*/
void date::addWeeks(int nweek)
{
    _start.addWeeks(nweek);
    _end.addWeeks(nweek);
}

/*
* adds a given number of months to this times
*/
void date::addMonths(int nmonth)
{
    _start.addMonth(nmonth);
    _end.addMonth(nmonth);
}

/*
* adds a given number of years to this times
*/
void date::addYears(int nyear)
{
    _start.addYears(nyear);
    _end.addYears(nyear);
}

/*
* returns the ID of this date object
*/
int date::getID()
{
    return _id;
}

/*
* returns the start of this date object
*/
datetime date::getStart()
{
    return _start;
}

/*
* returns the end of this date object
*/
datetime date::getEnd()
{
    return _end;
}

/*
* returns the consumer of this date object
*/
int date::getConsumer()
{
    return _consumer;
}

/*
* returns the comment of this date object
*/
std::string date::getComment()
{
    return _comment;
}

/*
* returns the type of this date object
*/
int date::getType()
{
    return _type;
}

/*
* returns repeat of this date object
*/
int date::getRepeat()
{
    return _repeat;
}
