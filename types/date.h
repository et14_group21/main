#pragma once

#include <string>

#include "datetime.h"

class date
{
    public:
        date();
        date(datetime nstart, datetime nend, int nconsumer, std::string ncomment, int ntype, int nrepeat);
        date(int nID, datetime nstart, datetime nend, int nconsumer, std::string ncomment, int ntype, int nrepeat);
        bool isEmpty();
        void shift(int minutes);
        void addMinutes(int nminute);
        void addHours(int nhour);
        void addDays(int nday);
        void addWeeks(int nweek);
        void addMonths(int nmonth);
        void addYears(int nyear);
        int getID();
        datetime getStart();
        datetime getEnd();
        int getConsumer();
        std::string getComment();
        int getType();
        int getRepeat();

    private:
        int _id;
        datetime _start;
        datetime _end;
        int _consumer;
        std::string _comment;
        int _type;
        int _repeat;
        bool _is_empty;
};
