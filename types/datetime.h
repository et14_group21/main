#pragma once

#include <string>
#include <ctime>

class datetime
{
    public:
        datetime();
        datetime(int nyear, int nmonth, int nday, int nhour, int nminute);
        std::string toString();
        std::tm toTimeStruct();
        void addMinutes(int mintoadd);
        void addHours(int hourtoadd);
        void addDays(int daytoadd);
        void addWeeks(int weektoadd);
        void addMonth(int monthtoadd);
        void addYears(int yeartoadd);
        datetime tillNow();
        bool operator <(const datetime& d);
        bool operator >(const datetime& d);
        bool operator <=(const datetime& d);
        bool operator >=(const datetime& d);
        datetime operator -(const datetime& d);
        static datetime getCurrent();
        int getYear();
        int getMonth();
        int getDay();
        int getHour();
        int getMinute();
        void setYear(int nyear);
        void setMonth(int nmonth);
        void setDay(int nday);
        void setHour(int nhour);
        void setMinute(int nminute);

    private:
        int _day;
        int _month;
        int _year;
        int _hour;
        int _minute;
        void OverwriteDatetime(std::tm timeStruct);
};
