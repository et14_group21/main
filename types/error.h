#pragma once

#include <string>

#include "datetime.h"

class error
{
    public:
        error();
        error(datetime ntimestamp, int ntype, std::string nname, int ncode);
        error(int ntype, std::string nname, int ncode);
        datetime getTimestamp();
        int getType();
        std::string getName();
        int getCode();

    private:
        datetime _timestamp;
        int _type;
        std::string _name;
        int _code;
};
