#pragma once

class switch_conf
{
    public:
        switch_conf(int Number, int Status, int Time);
        int getNumber();
        int getStatus();
        int getTime();
    
    private:
        int _number;
        int _status;
        int _time;
};