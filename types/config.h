#pragma once

class config
{
    public:
        config(bool SpeechOut, bool RemoteControl);
        bool getSpeechOut();
        bool getRemoteControl();
    
    private:
        bool _speech_out;
        bool _remote_control;
};