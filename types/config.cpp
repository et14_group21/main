#include "config.h"

config::config(bool SpeechOut, bool RemoteControl)
{
    _speech_out = SpeechOut;
    _remote_control = RemoteControl;
}

bool config::getSpeechOut()
{
    return _speech_out;
}

bool config::getRemoteControl()
{
    return _remote_control;
}