#include "consumer.h"

consumer_handler::consumer_handler(int consumerNumber)
{
    _consumerNumber = consumerNumber;
    //initialize random generator
    srand(time(NULL));
}

/*
* gets a random time to shift the next date if its in vacation mode
*/
void consumer_handler::next_shift()
{
    _randomShift = (rand() % 11) - 5;
}

/*
* checks what kind of repetition we have and writes new date to database
*/
void consumer_handler::repeat_handler(date Repeating)
{
    switch(Repeating.getRepeat())
    {
        case 1:
            Repeating.addDays(1);
            break;
        case 2:
            Repeating.addWeeks(1);
            break;
        case 3:
            Repeating.addMonths(1);
            break;
        case 4:
            Repeating.addYears(1);
            break;
    }
    
    //write new date to database
    database session;
    if(!session.isconnected())
    {
        // Database connection error
        return;
    }
    
    session.writeDate(Repeating);
}

/*
* turns consumer on till a spesific date
*/
void consumer_handler::turn_on_consumer(std::tm* off_time)
{
    //turn the consumer on
    consumer::turnON(_consumerNumber);
    
    //sleep this thread till the end of the date
    std::this_thread::sleep_until(std::chrono::system_clock::from_time_t(std::mktime(off_time)));
    
    //turn the consumer off
    consumer::turnOFF(_consumerNumber);
    
    //get a new random number in case we need it
    next_shift();
}

/*
* loop handler for date checking and consumer switching
*/
int consumer_handler::loop_handler()
{
    while(true)
    {
        //load next Date for the consumer
        database session;
        if(!session.isconnected())
        {
            // Database connection error
            return -1;
        }
        
        date _nextDate = session.getnextDate(_consumerNumber);
        date _nextVacation = session.getnextVacation();
        datetime _now = datetime::getCurrent();
        
        //check if there is actually a next Vacation
        if(!_nextVacation.isEmpty())
        {
            //check if the current date is in vacation mode
            if(_nextDate.getStart() >= _nextVacation.getStart() && _nextDate.getStart() <= _nextVacation.getEnd()){
                //add the random shift to the date
                _nextDate.shift(_randomShift);
            }
        }
        
        //check if there is actually a next data
        if(!_nextDate.isEmpty())
        {
            //check if the next date is now (in this minute)
            if(_now >= _nextDate.getStart() && _now <= _nextDate.getEnd())
            {
                //turn on the consumer till the date ends
                std::tm _end = _nextDate.getEnd().toTimeStruct();
                turn_on_consumer(&_end);
                
                //check if date was a repeating event
                if(_nextDate.getRepeat() != 0)
                {
                    repeat_handler(_nextDate);
                }
            }
        }
        
        //wait a bit and then start polling again
        std::this_thread::sleep_for(std::chrono::seconds(5));
    } 
}

/*
* entry point for new thread
*/
void consumer_handler::operator()()
{
   while(true)
   {
        //if this function returns an error happened:
        //log the error, wait a few seconds and try again
        int retValue = loop_handler();
        switch(retValue)
        {
            case -1:
                //LOG Database Connection error
                break;
            default:
                //LOG this should not happen
                break;
        }
        std::this_thread::sleep_for(std::chrono::seconds(5));
   } 
}