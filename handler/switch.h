#pragma once

#include <thread>
#include <chrono>

#ifdef MOCK_INPUT
    #include "../input/mock_input.h"
#else
    #include "../input/real_input.h"
#endif

#include "../types/switch_conf.h"
#include "../consumer/consumer.h"
#include "../files.h"

const int S_DISABLED = 0;
const int S_BUTTON = 1;
const int S_SWITCH = 2;
const int S_TIMER = 3;

class switch_handler {
    public:
        switch_handler(int number);
        void operator()();

    private:
        void getSettings();
        void buttonHandler();
        void switchHandler();
        void timerHandler();
        int _switchNumber;
        int _switchSetting;
        int _timerDuration;
        bool _working;
        
#ifdef MOCK_INPUT
        mock_input* _input;
#else
        real_input* _input;
#endif
};
