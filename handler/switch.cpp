#include "switch.h"

switch_handler::switch_handler(int number)
{
    _switchNumber = number;
    _working = false;
#ifdef MOCK_INPUT
    _input = new mock_input(number);
#else
    _input = new real_input(number);
#endif
}

/*
* handles switch mode 'button'
*/
void switch_handler::buttonHandler()
{
    consumer::turnONprio(_switchNumber);

    //while button is held (poll button)
    while(_input->pressed())
    {
        //sleep here
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    consumer::turnOFFprio(_switchNumber);

}

/*
* handles switch mode 'switch'
*/
void switch_handler::switchHandler()
{
    if(_working)
    {
      consumer::turnOFFprio(_switchNumber);
      _working = false;
    }
    else
    {
      consumer::turnONprio(_switchNumber);
      _working = true;
    }
}

/*
* handles switch mode 'timer'
*/
void switch_handler::timerHandler()
{
    consumer::turnONprio(_switchNumber);

    //sleep for set seconds
    std::this_thread::sleep_for(std::chrono::seconds(_timerDuration));

    consumer::turnOFFprio(_switchNumber);
}

/*
* entry point for new thread
*/
void switch_handler::operator()()
{
    while(true)
    {
        // dont let user change settings while consumer is still on from switch
        if(!_working)
        {
            getSettings();
        }

        //check if button is pressed
        if(_input->risingHappened())
        {
            switch(_switchSetting)
           {
                case S_DISABLED:
                    break;
                case S_BUTTON:
                    buttonHandler();
                    break;
                case S_SWITCH:
                    switchHandler();
                    break;
                case S_TIMER:
                    timerHandler();
                    break;
                default:
                    break;
           }
        }
        
        // wait a bit
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}

/*
* gets the current settings from file
*/
void switch_handler::getSettings()
{
    switch_conf _current = files::readSwitchConfig(_switchNumber);
    _switchSetting = _current.getStatus();
    _timerDuration = _current.getTime();
}
