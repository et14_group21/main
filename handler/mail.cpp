#include "mail.h"

/*
* default constructor
*/
mail_handler::mail_handler()
{
   
}

/*
* parses a mail subject by getting the consumer number and the desired status
* returns if the subject could be parsed
*/
bool mail_handler::parseSubject(std::string* Subject, int* Consumer, bool* Status)
{
   std::string _consumer = "Verbraucher";
   
   size_t _start = Subject->find(_consumer);
   
   // check if a consumer was specified
   if(_start == std::string::npos)
      return false;
   _start += (_consumer.length()+1);
   
   std::string _cons = Subject->substr(_start, 1);
   // check if the mail has the number at the right place
   if (!std::isdigit(_cons[0]))
      return false;
   
   *Consumer = std::stoi(_cons);
   
   bool _on = (Subject->find("an") != std::string::npos);
   bool _off = (Subject->find("aus") != std::string::npos);
   
   // check if a status was given
   if(!_on && !_off)
      return false;
   
   if(_on & !_off) { *Status = true; }
   else { *Status = false; }
   
   return true;
}

/*
* handles the polling for new mails and switching the consumer
*/
bool mail_handler::loopHandler()
{
   std::string _user;
   std::string _pass;
   files::readMailConfig(&_user, &_pass);
   std::string _lastMail;
   
   while(true)
   {
      mail_client _mc(_user, _pass);
      std::string _mail;
      if(! _mc.loadOldestMail(&_mail))
         break;
      _lastMail = _mail;
      _mc.deleteOldestMail();
   }
   std::string _subject = mail_client::getMailSubject(&_lastMail);
   
   int _consumerNo;
   bool _consumerStat;
   if(!parseSubject(&_subject, &_consumerNo, &_consumerStat))
      return false;
   
   if(_consumerStat)
      consumer::turnON(_consumerNo);
   else
      consumer::turnOFF(_consumerNo);
   
   return true;
}

/*
* entry point for new thread
*/
void mail_handler::operator()()
{
   while(true)
   {
      config _current = files::readConfig();
      if(_current.getRemoteControl())
      {
         loopHandler();
         std::this_thread::sleep_for(std::chrono::seconds(10));
      }
      else
      {
         std::this_thread::sleep_for(std::chrono::seconds(30));   
      }
   } 
}