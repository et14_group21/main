#pragma once

#include <thread>
#include <chrono>
#include <string>

#include "../types/config.h"
#include "../files.h"
#include "../mail/mail_client.h"
#include "../consumer/consumer.h"

class mail_handler
{
  public:
    mail_handler();
    void operator()();
  
  private:
    bool parseSubject(std::string* Subject, int* Consumer, bool* Status);
    bool loopHandler();
  
};