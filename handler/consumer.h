#pragma once

#include <thread>
#include <chrono>
#include <ctime>
#include <random>

#include "../types/date.h"
#include "../types/datetime.h"
#include "../database/database.h"
#include "../consumer/consumer.h"

class consumer_handler
{
    public:
        consumer_handler(int consumerNumber);
        void operator()();
    
    private:
        void turn_on_consumer(std::tm* off_time);
        int loop_handler();
        void next_shift();
        void repeat_handler(date Repeating);
        int _consumerNumber;
        int _randomShift;
};