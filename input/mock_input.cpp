#include "mock_input.h"

mock_input::mock_input(int number)
{
    _number = number;
    _fallingHappened = false;
    _risingHappened = false;
    _pressed = false;
    std::thread _poll_thread(&mock_input::poll, this, this);
    _poll_thread.detach();
}

int mock_input::readFile()
{
    std::string _filename = "input" + std::to_string(_number) + ".txt";
    std::ifstream _inputFile;
    _inputFile.open(_filename);
    
    int status = 0;
    
    if(_inputFile.is_open())
    {
        _inputFile >> status;
    }
    
    _inputFile.close();
    
    return status;
}

bool mock_input::pressed()
{
    return _pressed;
}

bool mock_input::fallingHappened()
{
    bool _happened = _fallingHappened;
    _fallingHappened = false;
    return _happened;
}

bool mock_input::risingHappened()
{
    bool _happened = _risingHappened;
    _risingHappened = false;
    return _happened;
}

void mock_input::reset()
{
    _fallingHappened = false;
    _risingHappened = false;
}

void mock_input::poll(mock_input* Access)
{
    int c_status = 1;
    int l_status;
    
    while(true)
    {
        l_status = c_status;
        c_status = readFile();
        
        if(l_status > c_status)
        {
            Access->_fallingHappened = true;
            Access->_pressed = true;
        }
        
        if(l_status < c_status)
        {
            Access->_risingHappened = true;  
            Access->_pressed = false;
        }
        
        if(Access->_fallingHappened && Access->_risingHappened)
        {
            Access->_fallingHappened = false;
            Access->_risingHappened = false;
            Access->_pressed = false;
        }
        
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}