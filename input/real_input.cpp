#include "real_input.h"
/*
* const that takes the switch number
*/
real_input::real_input(int Switch)
{
    _number = Switch;
    _switch = new gpio_switch(_number);
    _risingHappened = false;
    _pressed = false;
    
    std::thread _poll_thread(&real_input::poll, this, this);
    _poll_thread.detach();
}

/*
* returns if an event has happened and resets the status
*/
bool real_input::risingHappened()
{
    bool _happened = _risingHappened;
    _risingHappened = false;
    return _happened;
}

/*
* returns if the button is pressed currently
*/
bool real_input::pressed()
{
    return _pressed;
}

/*
* function to check the gpio pin
*/
void real_input::poll(real_input* Access)
{
    int c_status = 0;
    int l_status = 0;
    
    while(!Access->_switch->init())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    
    while(true)
    {
        l_status = c_status;
        c_status = Access->_switch->getStatus();
        
        if(l_status < c_status)
        {
            Access->_risingHappened = true;
            Access->_pressed = true;
        }
        
        if(l_status > c_status)
        {
            Access->_risingHappened = false;
            Access->_pressed = false;
        }
        
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}
