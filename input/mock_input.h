#pragma once

#include <thread>
#include <chrono>
#include <fstream>


class mock_input{
    public:
        mock_input(int number);
        bool pressed();
        bool fallingHappened();
        bool risingHappened();
        void reset();
        void poll(mock_input* _access);
        
    private:
        int readFile();
        int _number;
        bool _fallingHappened;
        bool _risingHappened;
        bool _pressed;
        std::thread _poll;
    
};