#pragma once

#include <thread>
#include <chrono>

#include "../gpio/gpio_switch.h"

class real_input{
    public:
        real_input(int Switch);
        bool pressed();
        bool risingHappened();
        
    private:
        int _number;
        gpio_switch* _switch;
        bool _risingHappened;
        bool _pressed;
        std::thread _poll;
        void poll(real_input* _access);
    
};