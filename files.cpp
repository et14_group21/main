#include "files.h"

const std::string CONFIG_FILE = "../config/config.json";
const std::string STATE_FILES = "../config/state_s";
const std::vector<std::string> SWITCH_FILES = { "../config/switch1.json", "../config/switch2.json"};
const std::string MAIL_FILE = "../config/mail.json";

json files::readJSON(std::string Filename)
{
    json f_json;
    std::ifstream ifs(Filename, std::ifstream::in);
    ifs >> f_json;
    
    return f_json;
}

config files::readConfig()
{
    json _config = readJSON(CONFIG_FILE);
    
    bool _speech_out        = _config["speech_out"].get<bool>();
    bool _remote_control    = _config["remote_control"].get<bool>();
    
    config _current(_speech_out, _remote_control);
    return _current;
}

switch_conf files::readSwitchConfig(int Number)
{
    json sConf_json = readJSON(SWITCH_FILES[Number-1]);
    
    int _status = sConf_json["status"].get<int>();
    int _time   = sConf_json["time"].get<int>();
    
    switch_conf _current(Number, _status, _time);
    return _current;
}

void files::readMailConfig(std::string* User, std::string* Password)
{
    json _config = readJSON(MAIL_FILE);
    *User = _config["username"].get<std::string>();
    *Password = _config["password"].get<std::string>();
}

void files::writeState(int Consumer, bool State)
{
    std::ofstream _state;
    std::string _fileName = STATE_FILES + std::to_string(Consumer) + ".txt";
    _state.open (_fileName, std::ios::trunc);
    _state << State;
    _state.close();
}