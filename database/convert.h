#pragma once

#include <string>
#include <vector>
#include <iostream>

#include "../types/date.h"
#include "../types/error.h"
#include "../types/datetime.h"

class convert
{
    public:
        static datetime toDatetime(std::string dateString);
        static date toDate(std::vector<std::string> dateStrings);
        static error toError(std::vector<std::string> errorStrings);
        static std::vector<std::string> splitString(std::string toSplit, char delimiter);
};

