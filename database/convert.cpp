#include "convert.h"

/*
* converts string from database to Datetime format
*/
datetime convert::toDatetime(std::string dateString)
{
    datetime ndatetime;
    std::vector<std::string> splitted = splitString(dateString, ' ');
    std::vector<std::string> day = splitString(splitted[0], '-');
    std::vector<std::string> time = splitString(splitted[1], ':');

    ndatetime.setYear(stoi(day[0]));
    ndatetime.setMonth(stoi(day[1]));
    ndatetime.setDay(stoi(day[2]));
    ndatetime.setHour(stoi(time[0]));
    ndatetime.setMinute(stoi(time[1]));
    return ndatetime;
}

/*
* converts a string vector to Date format
*/
date convert::toDate(std::vector<std::string> dateStrings)
{
    if(dateStrings[0] == "")
    {
        date emptydate;
        return emptydate;
    }
    date ndate(stoi(dateStrings[0]), toDatetime(dateStrings[1]), toDatetime(dateStrings[2]), stoi(dateStrings[3]), dateStrings[4], stoi(dateStrings[5]), stoi(dateStrings[6]));

    return ndate;
}

/*
* converts a string vector to Error format
*/
error convert::toError(std::vector<std::string> errorStrings)
{
    error nerror(toDatetime(errorStrings[1]), stoi(errorStrings[2]), errorStrings[3], stoi(errorStrings[4]));
    return nerror;
}

/*
* splits a given string at all of one given delimiter and returns a vector of these substrings
*/
std::vector<std::string> convert::splitString(std::string toSplit, char delimiter)
{
    std::vector<std::string> substrings;
    unsigned int position = 0;
    int length = 0;
    for (unsigned int i = 0; i < toSplit.length(); ++i)
    {
        if (toSplit[i] == delimiter)
        {
            length = i - position;
            std::string substring = toSplit.substr(position, length);
            substrings.push_back(substring);
            position = i + 1;
        }
    }
    if (position < toSplit.length())
    {
        std::string substring = toSplit.substr(position);
        substrings.push_back(substring);
    }
    return substrings;
}
