#include <vector>

#include "database.h"

/*
* Constructor
* connects to mysql database
*/
database::database()
{
    mysql_init(&_mysql);
    _connection = mysql_real_connect(&_mysql,"localhost","timerUser","group21","timer",3306,0,0);
}

/*
* Destructor
* closes database connection
*/
database::~database()
{
    mysql_close(_connection);
}

/*
* returns if connection was successful
*/
bool database::isconnected()
{
    return(_connection != NULL);
}

/*
* reads given mysql command from database and returns a vector with database content
*/
std::vector<std::string> database::readDatabase(std::string sql)
{
    mysql_query(_connection,sql.c_str());
    MYSQL_RES* result = mysql_store_result(_connection);
    MYSQL_ROW row = mysql_fetch_row(result);
    unsigned int length = mysql_num_fields(result);
    std::vector<std::string> strings = rowtoVector(row,length);
    mysql_free_result(result);
    return strings;
}

/*
* returns the next date in the database for the given consumer
*/
date database::getnextDate(int consumerNumber)
{
    date nextDate;
    std::string sql = "SELECT * FROM timer.dates WHERE consumer = " + std::to_string(consumerNumber) + " AND end > NOW() ORDER BY start ASC LIMIT 1";
    // next line: error, if connection gets interrupted
    std::vector<std::string> strings = readDatabase(sql.c_str());
    nextDate = convert::toDate(strings);
    return nextDate;
}

/*
* returns the next vacation in the database
*/
date database::getnextVacation()
{
    date nextVacation;
    std::string sql = "SELECT * FROM timer.dates WHERE type = 2 AND end > NOW() ORDER BY start ASC LIMIT 1";
    // next line: error, if connection gets interrupted
    std::vector<std::string> strings = readDatabase(sql.c_str());
    nextVacation = convert::toDate(strings);
    return nextVacation;
}

/*
* returns a vector of strings which contains the elements of a given mysql row with a given length
*/
std::vector<std::string> database::rowtoVector(MYSQL_ROW rowIn, unsigned int length)
{
    std::vector<std::string> strings;
    if (rowIn == NULL)
    {
        strings.push_back("");
        return strings;
    }
    for (unsigned int i = 0; i < length; ++i)
    {
        strings.push_back(rowIn[i]);
    }
    return strings;
}

/*
* writes a given error into the database
* returns if writing was successful
*/
bool database::writeError(error nerror)
{
    std::string sql = "INSERT INTO timer.error(timestamp,type,name,code) VALUES ( '" + nerror.getTimestamp().toString() + "', '" + std::to_string(nerror.getType()) + "', '" + nerror.getName() + "', '" + std::to_string(nerror.getCode()) + "')";
    if(mysql_query(_connection, sql.c_str()))
        return false;
    return true;
}

/*
* write a given date into the database
* returns if writing was successful
*/
bool database::writeDate(date ndate)
{
    std::string sql = "insert into `timer`.`dates` (`start`, `end`, `consumer`, `comment`, `type`, `repeat`) Values ( '" + ndate.getStart().toString() + "', '" + ndate.getEnd().toString() + "', '" + std::to_string(ndate.getConsumer()) + "', '" + ndate.getComment() + "', '" + std::to_string(ndate.getType()) + "', '" + std::to_string(ndate.getRepeat()) + "')";
    if(mysql_query(_connection, sql.c_str()))
        return false;
    return true;
}

