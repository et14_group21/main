#pragma once

#include <string>

#include <mysql/mysql.h>

#include "../types/date.h"
#include "convert.h"

class database
{
    public:
        database();
        ~database();
        date getnextDate(int consumerNumber);
        date getnextVacation();
        bool isconnected();
        std::vector<std::string> readDatabase(std::string sql);
        std::vector<std::string> rowtoVector(MYSQL_ROW rowIn, unsigned int length);
        bool writeError(error nerror);
        bool writeDate(date ndate);

    private:
        MYSQL* _connection;
        MYSQL _mysql;
};
