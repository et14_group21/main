#include <thread>
#include <iostream>

#include "handler/consumer.h"
#include "handler/switch.h"
#include "handler/mail.h"

using namespace std;

int main()
{
    switch_handler h_switch1(1);
    switch_handler h_switch2(2);
    cout << "Created Switch Handler" << endl;
    
    consumer_handler h_consumer1(1);
    consumer_handler h_consumer2(2);
    cout << "Created Consumer Handler" << endl;
    
    mail_handler h_mail;
    cout << "Created Mail Handler" << endl;
    
    thread switch1(h_switch1);
    cout << "Started Switch 1 Thread" << endl;
    thread switch2(h_switch2);
    cout << "Started Switch 2 Thread" << endl;

    thread consumer1(h_consumer1);
    cout << "Started Consumer 1 Thread" << endl;
    thread consumer2(h_consumer2);
    cout << "Started Consumer 2 Thread" << endl;
    
    thread mail(h_mail);
    cout << "Started Mail Thread" << endl;
    
    if(switch1.joinable())
    {
        switch1.join();
        cout << "Thread Switch 1 stopped" << endl;
    }
    if(switch2.joinable())
    {
        switch2.join();
        cout << "Thread Switch 2 stopped" << endl;  
    }
    
    if(consumer1.joinable())
    {
        consumer1.join();
        cout << "Thread Consumer 1 stopped" << endl;
    }
    if(consumer2.joinable())
    {
        consumer2.join();
        cout << "Thread Consumer 2 stopped" << endl;  
    }
    if(mail.joinable())
    {
        mail.join();
        cout << "Thread Mail stopped" << endl;  
    }
    
    return 0;
}