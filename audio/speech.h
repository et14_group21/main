#pragma once

#include <string>
#include <thread>
#include <chrono>
#include <vlc/vlc.h>

class speech
{
  public:
    static void playConsumerON(int Number);
    static void playConsumerOFF(int Number);
  private:
    static void playSound(std::string Path);
  
};