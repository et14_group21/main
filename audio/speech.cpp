#include "speech.h"

const std::string VOICE_OUT_ON[3] = {"", "audio/con1on.wav", "audio/con2on.wav"};
const std::string VOICE_OUT_OFF[3] = {"", "audio/con1off.wav", "audio/con2off.wav"};

/*
* manages vlc instance and play sound
*/
void speech::playSound(std::string Path)
{
    libvlc_instance_t* inst;
    libvlc_media_player_t* mp;
    libvlc_media_t* m;
    
    // load the vlc engine
    inst = libvlc_new(0, NULL);
    
    // create a new item
    m = libvlc_media_new_path(inst, Path.c_str());
    
    //get the information about the mitem
    libvlc_media_parse(m);
    libvlc_time_t _duration = libvlc_media_get_duration(m);
    
    // create a media play playing environment
    mp = libvlc_media_player_new_from_media(m);
    
    // no need to keep the media now
    libvlc_media_release(m);
    
    // play the media_player
    libvlc_media_player_play(mp);
    std::this_thread::sleep_for(std::chrono::milliseconds(_duration));
    
    // stop playing
    libvlc_media_player_stop(mp);
    
    // free the media_player
    libvlc_media_player_release(mp);
    libvlc_release(inst);
}

/*
* play the audio for consumer tun on
*/
void speech::playConsumerON(int Number)
{
    playSound(VOICE_OUT_ON[Number]);
}

/*
* play the audio for consumer tun on
*/
void speech::playConsumerOFF(int Number)
{
    playSound(VOICE_OUT_OFF[Number]);
}