#include "log.h"

void log::writeError(error Error_to_write)
{
    database db;
    if(db.isconnected())
    {
        db.writeError(Error_to_write);
    }
}

void log::writeLog(std::string name, ERROR_CODE errcode)
{
    datetime aktdate = datetime::getCurrent();
    error newError(aktdate, errcode/100, name, errcode);
    writeError(newError);
    
}
