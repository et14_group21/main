#pragma once

#include "database/database.h"
#include "types/error.h"

class log
{
    public:
        enum ERROR_CODE
        {
            C_1_OFF     = 110,
            C_1_ON		= 111,
            C_2_OFF     = 120,
            C_2_ON      = 121,
            E_UNKNOWN   = 400,
            E_DB_CON   	= 401
        };
        static void writeLog(std::string name, ERROR_CODE errcode);
        static void writeError(error Error_to_write);
};