#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "external/json/src/json.hpp"
#include "types/config.h"
#include "types/switch_conf.h"

using json = nlohmann::json;

class files
{
  public:
    static config readConfig();
    static switch_conf readSwitchConfig(int Number);
    static void readMailConfig(std::string* User, std::string* Password);
    static void writeState(int Consumer, bool State);
    
  private:
    static json readJSON(std::string Filename);
};