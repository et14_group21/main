#pragma once

#include <iostream>
#include <thread>
#include <chrono>

#include "../types/error.h"
#include "../types/config.h"
#include "../files.h"
#include "../audio/speech.h"
#include "../gpio/gpio_consumer.h"

class consumer
{
    public:
        static void turnON(int number);
        static void turnOFF(int number);
        static void turnONprio(int number);
        static void turnOFFprio(int number);
        static bool _c1_SWITCH;
        static bool _c2_SWITCH;
        
    private:
        static void ON(int number);
        static void OFF(int number);
        static void speechON(int number);
        static void speechOFF(int number);
        static error createError(bool ON, int number);
        static void writeState(int state, int number);
};