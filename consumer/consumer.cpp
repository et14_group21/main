#include "consumer.h"

#include "../files.h"
#include "../log.h"

using namespace std;

bool consumer::_c1_SWITCH = false;
bool consumer::_c2_SWITCH = false;

void consumer::writeState(int state, int number)
{
    gpio_consumer _pin (number);
    _pin.init();
    _pin.setStatus(std::to_string(state));
}
error consumer::createError(bool ON, int number)
{
    int _type = 1;
    std::string _name = "Verbraucher " + std::to_string(number);
    int _code = 100 + (10 * number);
    if(ON)
    {
        _name += " an.";
        _code += 1;
    }
    else
    {
       _name += " aus.";
       _code += 0;
    }
    
    error _switchEvent(_type, _name, _code);
    return _switchEvent;
}

void consumer::ON(int number)
{
    std::cout << "Consumer " << number << " activated" << std::endl;
    files::writeState(number, true);
    log::writeError(createError(true, number));
    writeState(1, number);
    std::thread _play_thread(consumer::speechON, number);
    _play_thread.detach();
}

void consumer::OFF(int number)
{
    std::cout << "Consumer " << number << " deactivated" << std::endl;
    files::writeState(number, false);
    log::writeError(createError(false, number));
    writeState(0, number);
    std::thread _play_thread(consumer::speechOFF, number);
    _play_thread.detach();
}

void consumer::turnON(int number)
{
    switch(number)
    {
        case 1:
            if(!_c1_SWITCH)
            {
                ON(number);
            }
            break;
        case 2:
            if(!_c2_SWITCH)
            {
                ON(number);
            }
            break;
        default:
            break;
    }
}

void consumer::turnOFF(int number)
{
    switch(number)
    {
        case 1:
            if(!_c1_SWITCH)
            {
                OFF(number);
            }
            break;
        case 2:
            if(!_c2_SWITCH)
            {
                OFF(number);
            }
            break;
        default:
            break;
    }
}

void consumer::turnONprio(int number)
{
    switch(number)
    {
        case 1:
            _c1_SWITCH = true;
            break;
        case 2:
            _c2_SWITCH = true;
            break;
        default:
            break;
    }
    ON(number);
}

void consumer::turnOFFprio(int number)
{
    switch(number)
    {
        case 1:
            _c1_SWITCH = false;
            break;
        case 2:
            _c2_SWITCH = false;
            break;
        default:
            break;
    }
    OFF(number);
}

void consumer::speechON(int number)
{
    config _current = files::readConfig();
    if(_current.getSpeechOut())
    {
        speech::playConsumerON(number);
    }
}

void consumer::speechOFF(int number)
{
    config _current = files::readConfig();
    if(_current.getSpeechOut())
    {
        speech::playConsumerOFF(number);
    }
}