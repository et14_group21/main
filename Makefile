#First attempt of a makefile

CXX=g++
CXXFLAGS=-pthread -std=c++11
LDFLAGS=`mysql_config --libs` `curl-config --libs` -lvlc

SOURCES = $(wildcard *.cpp) $(wildcard */*.cpp)
OBJS = $(SOURCES:.cpp=.o)

# define the executable file 
MAIN = timer

all: $(MAIN)
	@echo  The programm has been compiled

$(MAIN): $(OBJS) 
	$(CXX) $(CXXFLAGS) -o $(MAIN) $(OBJS) $(LDFLAGS)

# this is a suffix replacement rule for building .o's from .c's
# it uses automatic variables $<: the name of the prerequisite of
# the rule(a .c file) and $@: the name of the target of the rule (a .o file) 
# (see the gnu make manual section about automatic variables)
.c.o:
	$(CXX) $(CXXFLAGS) -c $<  -o $@
	
.PHONY: clean

clean:
	$(RM) *.o  */*.o *~ $(MAIN)