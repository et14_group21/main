#include "mail_client.h"

const std::string MAIL_SERVER_URL = "imaps://imap.web.de";
const std::string MAIL_SERVER_INBOX = "imaps://imap.web.de/INBOX";
const std::string OLDEST_MAIL = "imaps://imap.web.de/INBOX/;UID=1";

size_t mail_client::WriteCallback(void* contents, size_t size, size_t nmemb, void* destination)
{
    // This function need to be static or else a segmentation fault will happen when
    // dereferencing the content pointer
    ((std::string*)destination)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

mail_client::mail_client(std::string Username, std::string Password)
{
    _curl = curl_easy_init();
    
    if(_curl) {
        // Set username and password 
        curl_easy_setopt(_curl, CURLOPT_USERNAME, Username.c_str());
        curl_easy_setopt(_curl, CURLOPT_PASSWORD, Password.c_str());
        
        // Set the mail server (hardcoded)
        //curl_easy_setopt(_curl, CURLOPT_URL, "imaps://imap.web.de/INBOX");
        
        // set write function
        curl_easy_setopt(_curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        //curl_easy_setopt(_curl, CURLOPT_WRITEDATA, &_readBuffer);
        
        _initialized = true;
    }
    else
    {
        _initialized = false;
    }
}

mail_client::~mail_client()
{
    /* Always cleanup */ 
    curl_easy_cleanup(_curl);
}

bool mail_client::isInitialized()
{
    return _initialized;
}

/*
* reads the oldest mail from the imap mail server
* returns wehenever there was a mail or not
*/
bool mail_client::loadOldestMail(std::string* Buffer)
{
    CURLcode res = CURLE_OK;
    *Buffer = "";
    
    // This will fetch message 1 from the user's inbox
    curl_easy_setopt(_curl, CURLOPT_URL, OLDEST_MAIL.c_str());
    
    // set write buffer
    curl_easy_setopt(_curl, CURLOPT_WRITEDATA, Buffer);
    
    // Perform the fetch
    res = curl_easy_perform(_curl);
    
    /* Check for errors */
    if (res != CURLE_OK || Buffer->empty()){ return false; }
    else { return true; }
    //return (res == CURLE_OK);
}

/*
* deletes the oldest mail from the imap mail server
* returns if a mail was deleted
*/
bool mail_client::deleteOldestMail()
{
    CURLcode res = CURLE_OK;
    std::string _readBuffer;
    bool _deleted = false;
    
    curl_easy_setopt(_curl, CURLOPT_URL, MAIL_SERVER_INBOX.c_str());
    
    //set write buffer
    curl_easy_setopt(_curl, CURLOPT_WRITEDATA, &_readBuffer);
    
    // Set the STORE command with the Deleted flag for message 1. Note that
    // you can use the STORE command to set other flags such as Seen, Answered,
    // Flagged, Draft and Recent.
    curl_easy_setopt(_curl, CURLOPT_CUSTOMREQUEST, "STORE 1 +Flags \\Deleted");
    
    /* Perform the custom request */ 
    res = curl_easy_perform(_curl);
    
    _deleted = !_readBuffer.empty();
    
    /* Check for errors */
    if(res != CURLE_OK)
    {
        //std::cerr << curl_easy_strerror(res) << std::endl;
        _deleted = false;
    }
    else {
        // Set the EXPUNGE command, although you can use the CLOSE command if you
        //don't want to know the result of the STORE */ 
        curl_easy_setopt(_curl, CURLOPT_CUSTOMREQUEST, "EXPUNGE");
        
        // Perform the second custom request
        res = curl_easy_perform(_curl);
        
        // Check for errors
        if(res != CURLE_OK)
        {
          //std::cerr << curl_easy_strerror(res) << std::endl;
          //Is this important to check?
        }
    }
    return _deleted;
}

/*
* parses the subject forom a mail given as a std::string
*/
std::string mail_client::getMailSubject(std::string* Content)
{
    std::string _first_point = "Subject:";
    std::string _last_point = "From:";
    
    if(!Content->empty())
    {
        size_t _start = Content->find(_first_point)+_first_point.length()+1;
        size_t _end = Content->find(_last_point);
        size_t _length = _end-_start-2;
    
        return Content->substr(_start, _length);
    }
    else
    {
        return "";
    }
}