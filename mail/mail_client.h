#pragma once

#include <string>
#include <curl/curl.h>

class mail_client
{
  public:
    mail_client(std::string Username, std::string Password);
    ~mail_client();
    bool isInitialized();
    bool loadOldestMail(std::string* Buffer);
    bool deleteOldestMail();
    static std::string getMailSubject(std::string* Content);
    
  private:
    CURL* _curl;
    bool _initialized;
    static size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* destination);
};